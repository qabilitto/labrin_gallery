from django.shortcuts import render, redirect, reverse
from django.views.generic import ListView, TemplateView, View
from django.contrib.auth.mixins import LoginRequiredMixin
from . import models
from . import tasks
import json
# Create your views here.

class ImageListView(LoginRequiredMixin, ListView):
    login_url = 'login'
    model = models.Images
    template_name = 'photo_app/home.html'
    context_object_name = 'images'
    def get_queryset(self):
        queryset = self.model.objects.filter(image_owner_id = self.request.user.pk)
        return queryset
    
    def post(self, request):
        image = self.request.FILES['image']
        self.model.objects.create(image=image,image_owner=self.request.user, image_thumbnail='/thumbnails/thumbnail.'+str(image))
        result = tasks.cropimage.delay(str(image))
        result.get() 
        return redirect(reverse('home'))

class IndexView(TemplateView):
    template_name = 'photo_app/index.html'
