from django.db import models
from photo_drive import settings
from django.contrib.auth.models import User
# Create your models here.


class Images(models.Model):
    image = models.ImageField(upload_to='')
    image_type = models.CharField(max_length=10, null=True)
    image_owner=models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    image_thumbnail = models.ImageField(upload_to='thumbnails/', null=True)
    class Meta:
        db_table = 'images_table'

class Comments(models.Model):
    image_comment = models.ForeignKey(Images,on_delete=models.CASCADE)
    image_commentor = models.ForeignKey(User,on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    comment_content = models.TextField()
    
    class Meta:
        db_table= 'comments_table'

class User_Image(models.Model):
    img_id = models.IntegerField()
    user_id = models.IntegerField()
    is_commentable = models.BooleanField(default=False)

    class Meta:
        db_table = 'user_image_table'