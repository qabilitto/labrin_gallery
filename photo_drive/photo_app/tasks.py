from PIL import Image
from photo_drive.celery import app

@app.task(name='cropper')
def cropimage(image):
    img = Image.open('media/'+str(image))
    size = 200, 400
    img.thumbnail(size)
    img_name = 'media/thumbnails/thumbnail.'+image
    img.save(img_name)