from django.shortcuts import render, redirect,reverse
from django.views.generic import View
from django.contrib.auth import login, authenticate
from django.contrib.auth.views import LoginView
from . import forms
# Create your views here.
class SignUpView(View):
    def post(self,request):
        form = forms.UserCreateForm(request.POST)
        if form.is_valid():
            user = form.save()
            print(user)
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect(reverse('home'))
        else:
            form = forms.UserCreateForm()
            return render(request, 'login/signup.html',{'form':form})
    def get(self, request):
        form = forms.UserCreateForm()
        #print(form.fields)
        return render(request, 'login/signup.html',{'form':form})
