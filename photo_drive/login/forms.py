from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User

class UserCreateForm(UserCreationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'email',
        ]
    
    def __init__(self, *args, **kwargs):
        super(UserCreateForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class':'form-control','placeholder':'user name'})
        self.fields['email'].widget.attrs.update({'class':'form-control','placeholder':'E-mail'})
        self.fields['password1'].widget.attrs.update({'class':'form-control','placeholder':'password'})
        self.fields['password2'].widget.attrs.update({'class':'form-control','placeholder':'repeart passwod'})

class AuthForm(AuthenticationForm):
    class Meta:
        model = User
        fields = [
            'username',
            'password',
        ]
    def __init__(self, *args, **kwargs):
        super(AuthForm,self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class':'form-control','placeholder':'user name'})
        self.fields['password'].widget.attrs.update({'class':'form-control','placeholder':'password'})